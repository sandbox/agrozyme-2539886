<?php

namespace Drupal\i18n_list;

use Drupal\mixin\Traits\Hook;

//use Drupal\i18n_list\Views\FilterFieldList;

class Views {
  use Hook;

  static function init() {
    static::createHooks();
    views_include('cache');
    _views_fetch_data_build();
  }

  static function hook_views_api() {
    return ['api' => 3];
  }

  static function hook_field_views_data_alter(&$result, $field, $module) {
    $info = field_info_formatter_types('select_or_other_formatter');

    if (FALSE == in_array($field['type'], $info['field types'])) {
      return;
    }

    $instances = static::getInstances($field);

    if (empty($instances)) {
      return;
    }

    $name = $field['field_name'] . '_value';
    $callback = static::getType()->getModule() . '_views_handler_options_list';
    $agruments = [$field['field_name']];

    foreach ($result as $index => $settings) {
      $filter = &$result[$index][$name]['filter'];
      $filter['handler'] = 'views_handler_filter_in_operator';
      $filter['options callback'] = $callback;
      $filter['options arguments'] = $agruments;
    }
  }

  static function getInstances($field) {
    $instances = [];

    foreach ($field['bundles'] as $type => $bundles) {
      foreach ($bundles as $bundle) {
        $instance = field_info_instance($type, $field['field_name'], $bundle);

        if ('select_or_other' == $instance['widget']['type']) {
          $instances[] = $instance;
        }
      }
    }

    return $instances;
  }

  static function hook_views_handler_options_list($field_name) {
    $options = [];
    $field = field_info_field($field_name);

    foreach (static::getInstances($field) as $instance) {
      $options += Multilingual::translateOptions($instance, Multilingual::prepareOptions($instance));
    }

    if (isset($options['other'])) {
      $other = ['other' => $options['other']];
      unset($options['other']);
      $options += $other;
    }

    return $options;
  }

  protected static function getHookMap() {
    $class = get_called_class();
    $module = static::getType()->getModule();
    $hooks = [];
    $items = ['views_api', 'field_views_data_alter', 'views_handler_options_list'];

    foreach ($items as $item) {
      $hooks[$class]['hook_' . $item] = $module . '_' . $item;
    }

    return $hooks;
  }

}

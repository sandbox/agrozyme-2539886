<?php
namespace Drupal\i18n_list;

use Drupal\mixin\Arrays;
use Drupal\mixin\Traits\Hook;

abstract class Base {
  use Hook;

  static function translateOptions($instance, array $available_options, $options = []) {
    if (empty($available_options)) {
      return [];
    }

    $callback = function ($index, $item) use ($instance, $options) {
      $name = static::getTranslateModule() . '#available_options#' . static::cleanName($index);
      $conetxt = static::getTranslateContext($instance, $name);
      return static::translate($conetxt, $item, $options);
    };

    return Arrays::map($available_options, $callback);
  }

  protected static function getTranslateModule() {
    return 'select_or_other';
  }

  protected static function cleanName($name) {
    return htmlentities(drupal_html_class(trim($name)));
  }

  protected static function getTranslateContext($instance, $name) {
    $names = ['field', $instance['field_name'], $instance['bundle'], $name];
    return implode(':', $names);
  }

  static function translate($name, $string, $options = []) {
    return function_exists('i18n_string') ? i18n_string($name, $string, $options) : $string;
  }

  static function translateOtherLabel($instance, $string, $options = []) {
    $name = static::getTranslateModule() . '#other#label';
    $context = static::getTranslateContext($instance, $name);
    return static::translate($context, $string, $options);
  }

  static function translateOtherTitle($instance, $string, $options = []) {
    $name = static::getTranslateModule() . '#other#title';
    $context = static::getTranslateContext($instance, $name);
    return static::translate($context, $string, $options);
  }

  static function hasFieldTranslation() {
    return module_exists('i18n_field');
  }

  static function prepareOptions($instance) {
    if (FALSE == isset($instance['widget']['settings']['available_options'])) {
      return [];
    }

    module_load_include('inc', 'select_or_other', 'select_or_other.field_widget');
    $field = field_info_field($instance['field_name']);
    return select_or_other_field_widget_form_prepare_options($field, $instance);
  }

}

<?php

namespace Drupal\i18n_list;

use Drupal\mixin\Arrays;
use Drupal\mixin\Traits\Hook;

class Field extends Base {
  use Hook;

  static function init() {
    static::createHooks();
    module_implements(NULL, FALSE, TRUE);
    field_info_cache_clear();
  }

  static function hook_field_widget_select_or_other_form_alter(&$element, &$form_state, $context) {
    if (FALSE == isset($element['#entity'])) {
      return;
    }

    $instance = $context['instance'];
    $element['#other'] = static::translateOtherLabel($instance, $element['#other']);
    $element['#other_title'] = static::translateOtherTitle($instance, $element['#other_title']);
    $element['#options'] = static::translateOptions($instance, $element['#options']);
  }

  static function hook_field_formatter_info_alter(&$info) {
    $info['select_or_other_formatter']['module'] = static::getType()->getModule();
  }

  static function hook_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
    $options = static::translateOptions($instance, static::prepareOptions($instance));

    $callback = function ($delta, $item) use ($instance, $options) {
      $index = $item['value'];
      $value = array_key_exists($index, $options) ? $options[$index] : $index;
      return ['#markup' => $value];
    };

    return Arrays::map($items, $callback);
  }

  protected static function getHookMap() {
    $class = get_called_class();
    $module = static::getType()->getModule();
    $hooks = [];
    $items = ['field_formatter_view', 'field_widget_select_or_other_form_alter', 'field_formatter_info_alter'];

    foreach ($items as $item) {
      $hooks[$class]['hook_' . $item] = $module . '_' . $item;
    }

    return $hooks;
  }

}

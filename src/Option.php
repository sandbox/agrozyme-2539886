<?php

namespace Drupal\i18n_list;

use Drupal\mixin\Arrays;
use Drupal\mixin\Caller;
use Drupal\mixin\Getter;
use Drupal\mixin\Traits\Hook;
use Drupal\mixin\Traits\Object;
use Drupal\mixin\Traits\PropertyBagRegistry;

abstract class Option {
  use Object, Hook, PropertyBagRegistry {
    Object::className insteadof Hook, PropertyBagRegistry;
    Object::getType insteadof Hook, PropertyBagRegistry;
    Object::toArray insteadof Hook, PropertyBagRegistry;
  }

  var $type = '';
  var $bundle = '';
  var $field = '';
  protected $options = [];

  static function hookOptions($entity_type, $bundle, $field_name) {
    $items = static::getAll();
    return Getter::create($items, [$entity_type, $bundle, $field_name])->fetch(FALSE);
  }

  static function getAll() {
    static $cache = [];

    $builder = function () {
      $items = [];

      foreach (static::setupMethodNames() as $name => $isStatic) {
        if (FALSE == $isStatic) {
          continue;
        }

        $items = array_merge_recursive($items, static::createItem($name));
      }

      return $items;
    };

    return Getter::create($cache, [get_called_class()])->setBuilder($builder)->fetch();
  }

  protected static function createItem($name) {
    $data = Caller::invoke([get_called_class(), $name], [], []);

    if (($data instanceof self)) {
      $items = [$data];
    } else if ((FALSE == is_array($data)) || (FALSE == empty($data))) {
      $items = $data;
    } else {
      return [];
    }

    $result = [];

    foreach ($items as $item) {
      if (($item instanceof self) && $item->vaildateIndex()) {
        $result[$item->type][$item->bundle][$item->field] = $item->getValues();
      }
    }

    return $result;
  }

  function vaildateIndex() {
    $setting = field_info_instance($this->type, $this->field, $this->bundle);
    return isset($setting);
  }

  function getValues() {
    $callback = function ($index, $item) {
      return $index . '|' . $item;
    };

    return Arrays::map($this->options, $callback);
  }

  protected static function setupMethodPattern() {
    return '/^option[A-Z]/';
  }

  static function create($type = '', $bundle = '', $field = '', array $options = []) {
    return new static(compact('type', 'bundle', 'field', 'options'));
  }

  protected static function getHookMap() {
    $module = static::getType()->getModule();
    $items = [get_called_class() => ['hookOptions' => $module . '_select_or_other_available_options']];
    return $items;
  }

}

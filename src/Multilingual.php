<?php

namespace Drupal\i18n_list;

use Drupal\mixin\Getter;
use Drupal\mixin\Traits\Hook;

class Multilingual extends Base {
  use Hook;

  static function hook_i18n_field_info_alter(&$info) {
    //$info['text']['translate_available_options'] = 'i18n_list_translate_available_options';
  }

  static function hook_i18n_string_list_field_alter(&$strings, $type = NULL, $object = NULL) {
    $module = Getter::create($object, ['widget', 'module'])->fetch(FALSE);

    if (('field_instance' != $type) || ('select_or_other' != $module)) {
      return;
    }

    $field_name = $object['field_name'];
    $element = &$strings['field'][$field_name][$object['bundle']];
    static::attachOther($element, $module, $object['widget']['settings']);
    static::attachOptions($element, $module, static::prepareOptions($object));
  }

  protected static function attachOther(&$element, $module, $settings) {
    $index = static::getTranslateModule() . '#other#';

    if (isset($settings['other'])) {
      $element[$index . 'label'] = ['title' => t('Other'), 'string' => $settings['other']];
    }

    if (isset($settings['other_title'])) {
      $element[$index . 'title'] = ['title' => t('Other Title'), 'string' => $settings['other_title']];
    }
  }

  protected static function attachOptions(&$element, $module, $settings) {
    foreach ($settings as $index => $item) {
      $index = static::getTranslateModule() . '#available_options#' . static::cleanName($index);
      $element[$index] = ['title' => t('Option %name', ['%name' => $item]), 'string' => $item];
    }
  }

  static function hook_i18n_list_translate_available_options($instance, $value, $langcode = NULL) {
    $options = ['langcode' => $langcode, 'sanitize' => FALSE];
    return static::translateOptions($instance, static::prepareOptions($instance), $options);
  }

  protected static function getHookMap() {
    $class = get_called_class();
    $module = static::getType()->getModule();
    $hooks = [];
    $items = [
      'i18n_field_info_alter',
      'i18n_string_list_field_alter',
      //'i18n_list_translate_available_options'
    ];

    foreach ($items as $item) {
      $hooks[$class]['hook_' . $item] = $module . '_' . $item;
    }

    return $hooks;
  }

}
